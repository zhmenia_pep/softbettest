<?php

namespace UsersModel;

use Doctrine\DBAL\DBALException;

class Users
{
    public function __construct($app){
        $this->conf = $app;
    }

    private function LogSqlErrors($message = null, $caller = null){
        $this->conf->db->insert('sql_errors_log', array('caller' => $caller, 'message' => $message));
    }

    private function isUserNameUsed($username = '', $user_id = ''){
        if (empty($username) || strlen($username) < 3) return false;

        $sql = "select * from users	where username = :username";
        if (is_numeric($user_id) && $user_id) $sql .= " and id <> " . intval($user_id);

        try {
            $statement = $this->conf->db->executeQuery($sql, ['username' => $username]);
            $data      = $statement->fetchAll();
            if (count($data) > 0) {
                return true;
            }else return false;
        } catch (DBALException $e) {
            $this->LogSqlErrors(sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage()), 'Users\isUserNameUsed') ;
            return true;
        }
    }

    public function register($data = array()){
        $rest_out = array('err' => null, 'err_m' => 'Invalid parameters', 'user_id' => null );
        if (empty($data) || (count($data) < 3)) return $rest_out;
        if (!isset($data['username'])
            || !isset($data['cnp']) || !isset($data['password'])
        ) {
            return $rest_out;
        }

        if ($this->isUserNameUsed($data['username'])) {
            $rest_out['err_m'] = "Username is already in use, please choose another one";
            return $rest_out;
        }

        try {
            $this->conf->db->insert('users', ['username' => $data['username'], 'cnp' => $data['cnp'], 'password' => md5($data['password'])]);
            $rest_out['user_id'] = $this->conf->db->lastInsertId();
        } catch (DBALException $e) {
            $this->LogSqlErrors(sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage()), 'Users\register') ;
            $rest_out = array('err' => 2800, 'err_m' => 'DB Error');
        }

        return $rest_out;
    }
}