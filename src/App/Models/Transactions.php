<?php

namespace TransactionsModel;

use Doctrine\DBAL\DBALException;

class Transactions
{
    public function __construct($app){
        $this->conf = $app;
        $this->transaction_obj = array(
                 'insertable_fields' => array('mandatory' => array('customerId' => 'numeric','amount' => 'numeric'), 'optional' => array('transactionId' => 'numeric'))
                ,'updatable_fields' => array('amount' => 'numeric')
                ,'queryable_fields' => array('transactionId', 'customerId', 'amount', 'date')
                ,'filterable_fields' => array('transactionId'=> 'transactionId', 'customerId' => 'customerId', 'amount' => 'amount', 'date' => 'CAST(`date` as date) = :date')
        );
    }

    private function LogSqlErrors($message = null, $caller = null){
        $this->conf->db->insert('sql_errors_log', array('caller' => $caller, 'message' => $message));
    }

    public function addTransaction($data = array(), $user = null){
        $err = array();
        $rest_out = array('err' => null, 'err_m' => 'Invalid parameters', 'transactionId' => null);
        $transaction_fields = array('creator_user_id' => $user['id']);
        foreach ($this->transaction_obj['insertable_fields'] as $category => $fields) {
            foreach ($fields as $field => $datatype) {
                if ($category == 'mandatory') {
                    if (!isset($data[$field])) {
                       array_push($err, 'Field ' . $field . ' is mandatory');
                       continue;
                    }
                }
                if ($datatype == 'numeric') {
                    if ($category == 'mandatory' && !is_numeric($data[$field])) {array_push($err, 'Field ' . $field . ' is mandatory, numeric'); continue;}
                    elseif (!empty($data[$field]) && !is_numeric($data[$field])) {array_push($err, 'Field ' . $field . ' should be numeric'); continue;}
                    elseif (!empty($data[$field])) $transaction_fields[$field] = $data[$field];
                    else continue;
                }
            }
        }

        if (count($err)) {
            $rest_out['err'] = 2900; $rest_out['err_m'] = 'Errors: ' . implode($err, ', ');
            return $rest_out;
        }

        try {
            $this->conf->db->insert('transactions', $transaction_fields);
            $rest_out['transactionId'] = $this->conf->db->lastInsertId();
        } catch (DBALException $e) {
            $this->LogSqlErrors(sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage()), 'Transactions\addTransaction') ;
            $rest_out = array('err' => 2800, 'err_m' => 'DB Error');
        }

        return $rest_out;
    }

    public function updateTransaction($data = array(), $transactionId = null, $user = null){
        $err = array();
        $rest_out = array('err' => null, 'err_m' => '', 'transactionId' => null);
        if (empty($transactionId) || empty($user) || empty($user['id'])) return $rest_out;

        $transaction_fields = array('updated_user_id' => $user['id'], 'updated_at' => 'NOW()');
        foreach ($this->transaction_obj['updatable_fields'] as $field => $datatype) {
            if ($datatype == 'numeric') {
                if (!empty($data[$field]) && !is_numeric($data[$field])) {array_push($err, 'Field ' . $field . ' should be numeric'); continue;}
                elseif (isset($data[$field]) && empty($data[$field])) $transaction_fields[$field] = null;
                else $transaction_fields[$field] = $data[$field];
            }
        }

        if (count($err)) {
            $rest_out['err'] = 2900; $rest_out['err_m'] = 'Errors: ' . implode($err, ', ');
            return $rest_out;
        }

        if (count($transaction_fields) < 3){
            $rest_out['err'] = 2900; $rest_out['err_m'] = 'Invalid parameters';
            return $rest_out;
        }

        try {
            $this->db->update('transactions', $transaction_fields, array('transactionId' => $transactionId));
        } catch (DBALException $e) {
            $this->LogSqlErrors(sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage()), 'Transactions\updateTransaction') ;
            $rest_out = array('err' => 2800, 'err_m' => 'DB Error');
        }

        return $rest_out;
    }

    public function getTransactions($data = array()){
        $rest_out = array('err' => null, 'err_m' => '', 'transactions' => array(), 'transactionsTotal' => 0, 'pagesCount' => 1, 'offset' => 0);

        $order_by = (!empty($data['orderBy']) && in_array($this->transaction_obj['filterable_fields'], $data['orderBy'])) ? " order by ". $data['orderBy'] . ' ' . $data['orderWay'] : " order by date desc";
        $data['offset'] = empty($data['offset']) ? 0 : intval($data['offset']);
        $data['limit'] = empty($data['limit']) ? 5 : intval($data['limit']);

        $transactions_filter = array();
        $additional_where = "";

        foreach ($this->transaction_obj['filterable_fields'] as $field => $cast) {
            if (!empty($data[$field])) {
                $transactions_filter[$field] = $data[$field];
                $casted = ($field == $cast) ? $field .' = :'.$field : $cast;
                $additional_where .= ' and ' . $casted;
            }
        }

        $sql = 'select ' . implode($this->transaction_obj['queryable_fields'], ', ') . ' from transactions where 1=1 '
            . $additional_where . $order_by . ' LIMIT ' . $data['offset']*$data['limit'] . ', ' . $data['limit'];

        try {
            $statement = $this->conf->db->executeQuery($sql, $transactions_filter);
            $res = $statement->fetchAll();
            if (count($res) || $data['offset'] == 0) {
                $rest_out['transactions'] = $res;
                $statement = $this->conf->db->executeQuery('select count(*) as overall from transactions where 1=1 ' . $additional_where, $transactions_filter);
                $res = $statement->fetchAll();
                $rest_out['transactionsTotal'] = $res[0]['overall'];
                $rest_out['pagesCount'] = (intval($res[0]['overall']) > 0 ) ? ceil(intval($res[0]['overall'])/$data['limit']) : 1;
                $rest_out['offset'] = $data['offset'];
            }
        } catch (DBALException $e) {
            $this->LogSqlErrors(sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage()), 'Transactions\getTransactions') ;
            $rest_out = array('err' => 2800, 'err_m' => 'DB Error');
        }

        return $rest_out;
    }

    public function delTransaction($transactionId = null){
        if (!intval($transactionId)) return array('err' => 2900, 'err_m' => 'transactionId parameter should be numeric');
        $rest_out = array('err' => null, 'err_m' => '');

        try {
            $res = $this->conf->db->delete('transactions', array('transactionId' => $transactionId));
            if (!$res) return array('err' => 2900, 'err_m' => 'Transaction with such Id not exists in DB');
        } catch (DBALException $e) {
            $this->LogSqlErrors(sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage()), 'Transactions\delTransaction') ;
            $rest_out = array('err' => 2800, 'err_m' => 'DB Error');
        }
        return $rest_out;
    }

    public function calcSumsOfTransactionsOverPrevDay($data = array()){
        $rest_out = array('err' => null, 'err_m' => '');

        if (empty($data['id']) || !intval($data['id'])) {
            $rest_out['err'] = 2900; $rest_out['err_m'] = "Invalid incoming parameters";
            return $rest_out;
        }

        $sql = 'insert into transactions_summary (date, amount, creator_user_id)'
                .' select cast(`date` as date), sum(amount), ? from transactions t'
                .' where cast(`date` as date) = cast(DATE_SUB(NOW(), INTERVAL 1 DAY) as date)'
                .'      and not exists (select null from transactions_summary where transactions_summary.`date` = cast(t.`date` as date) )'
                .' group by cast(`date` as date), ?';
        try {
            $statement = $this->conf->db->prepare($sql);
            $statement->bindValue(1, $data['id']);
            $statement->bindValue(2, $data['id']);
            $statement->execute();
        } catch (DBALException $e) {
            $this->LogSqlErrors(sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage()), 'Transactions\calcSumsOfTransactionsOverPrevDay') ;
            $rest_out = array('err' => 2800, 'err_m' => 'DB Error');
        }
        return $rest_out;
    }
}