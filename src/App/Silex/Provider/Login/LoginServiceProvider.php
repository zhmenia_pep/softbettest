<?php

namespace App\Silex\Provider\Login;

use Silex\Application;
use Silex\ServiceProviderInterface;

class LoginServiceProvider implements ServiceProviderInterface
{
    const AUTH_VALIDATE_CREDENTIALS = 'auth.validate.credentials';
    const AUTH_VALIDATE_TOKEN       = 'auth.validate.token';
    const AUTH_NEW_TOKEN            = 'auth.new.token';

    public function register(Application $app){
        $app[self::AUTH_VALIDATE_CREDENTIALS] = $app->protect(function ($user, $pass, $app) {
            return $this->validateCredentials($user, $pass, $app);
        });

        $app[self::AUTH_VALIDATE_TOKEN] = $app->protect(function ($token, $app) {
            return $this->validateToken($token, $app);
        });

        $app[self::AUTH_NEW_TOKEN] = $app->protect(function ($user, $app) {
            return $this->getNewTokenForUser($user, $app);
        });
    }

    public function boot(Application $app){
    }

    private function validateCredentials($user, $pass, $app){
        $statement = $app->db->executeQuery('select u.* from users u where u.username = :USERNAME and u.password = md5(:PASSWORD)', array('USERNAME' => $user, 'PASSWORD' => $pass));
        $data = $statement->fetchAll();
        if (count($data) > 0) {
            $app['user'] = $data[0];
            return true;
        }else return false;
    }

    private function validateToken($token, $app){
        $statement = $app->db->executeQuery('select u.* from access_tokens acc_t, users u where acc_t.access_token = :KEY and acc_t.user_id = u.id', ['KEY' => $token]);
        $data      = $statement->fetchAll();

        if (count($data) > 0) {
            $app['user'] = $data[0];
            return true;
        }else return false;
    }

    private function getNewTokenForUser($user, $app){
        $tmp = $app['user'];
        $tmp['created_at'] = time();
        $tmp['new_token'] = md5(base64_encode(serialize($tmp)));

        $app->db->insert('access_tokens', ['user_id' => $app['user']['id'], 'access_token' => $tmp['new_token']]);
        return $tmp['new_token'];
    }
}