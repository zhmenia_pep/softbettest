<?php

namespace App\Silex\Provider\Login;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException,
    Symfony\Component\HttpFoundation\Request,
    Silex\ControllerProviderInterface,
    Silex\Application;

class LoginControllerProvider implements ControllerProviderInterface
{
    const VALIDATE_CREDENTIALS = '/validateCredentials';
    const TOKEN_HEADER_KEY     = 'Authorization';
    const TOKEN_REQUEST_KEY    = 'Bearer ';

    private $baseRoute;

    public function setBaseRoute($baseRoute){
        $this->baseRoute = $baseRoute;
        return $this;
    }

    public function connect(Application $app){
        $this->setUpMiddlewares($app);
        return $this->extractControllers($app);
    }

    private function extractControllers(Application $app){
        $controllers = $app['controllers_factory'];

        $controllers->post(self::VALIDATE_CREDENTIALS, function (Request $request) use ($app) {
            $user   = $request->get('username');
            $pass   = $request->get('password');
            $status = $app[LoginServiceProvider::AUTH_VALIDATE_CREDENTIALS]($user, $pass, $app);
            if ($status) {
                $new_token = $app[LoginServiceProvider::AUTH_NEW_TOKEN]($user, $app);
                $app['session']->set('user', array('username' => $user, 'id' => $app['user']['id'], 'access_token' => $new_token));
            }else {
                $app['session']->set('user', array('username' => 'anonymous', 'id' => 0, 'access_token' => ''));

            }
            $user = array();
            try {
                $user = $app['session']->get('user');
            }catch (Exception $e) {

                $user =  array('username' => 'anonymous', 'id' => 0, 'access_token' => '');
                $app['session']->set('user', array('username' => 'anonymous', 'id' => 0, 'access_token' => ''));
            }

            if ($user['id']) {
                $params = array("user" => $user, "path" => $app->config['index_pathes']);
                return $app['twig']->render('index.html.twig', $params);
            }else {
                $params = array('error' => "Bad credentials, try again please");
                return $app['twig']->render('login.html.twig', $params);
            }
        });

        return $controllers;
    }

    private function setUpMiddlewares(Application $app){
        $app->before(function (Request $request) use ($app) {
            if (!$this->isAuthRequiredForPath($request->getPathInfo())) {
                if (!$this->isValidTokenForApplication($app, $this->getTokenFromRequest($request))) {
                    throw new AccessDeniedHttpException('Access Denied');
                }
                //$response_stored = $app['cache']->fetch($request_bcripted);
            }else {
                //$response_stored = $app['cache']->fetch($request_bcripted);
            }
        });

        $app->finish(function (Request $request, Response $response, Application $app) {
            //$app['cache']->store($request_bcripted, $response, 60*60);
        });
    }

    private function getTokenFromRequest(Request $request){
        return str_replace(self::TOKEN_REQUEST_KEY, '', $request->headers->get(self::TOKEN_HEADER_KEY, $request->get(self::TOKEN_REQUEST_KEY)));
    }

    private function isAuthRequiredForPath($path){
        return in_array($path, ['/', '/login', '/signup', '/logout', '/transactions', '/about', '/api/users/register', $this->baseRoute . self::VALIDATE_CREDENTIALS]);
    }

    private function isValidTokenForApplication(Application $app, $token){
        $statement = $app->db->executeQuery('select u.* from access_tokens acc_t, users u where acc_t.access_token = :KEY and acc_t.user_id = u.id', ['KEY' => $token]);
        $data      = $statement->fetchAll();
        if (count($data) > 0) {
            $app['user'] = $data[0];
            $app['session']->set('user', array('username' => $data[0]['username'], 'id' => $data[0]['id'], 'access_token' => $token));
        }else {
            $app['session']->set('user', array('username' => 'anonymous', 'id' => 0, 'access_token' => ''));
        }
        return $app[LoginServiceProvider::AUTH_VALIDATE_TOKEN]($token, $app);
    }
}