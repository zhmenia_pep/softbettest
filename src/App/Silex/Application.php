<?php


namespace App\Silex;


use Silex\Application as SilexApplication, Silex\Provider as SilexProvider;
use App\Silex\Provider\Login\LoginBuilder;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response,
    Silex\Provider\TwigServiceProvider,
    Silex\Provider\SessionServiceProvider,
    Moust\Silex\Provider,
    \Twig_SimpleFunction as TwigFunction;

use Doctrine\DBAL\DriverManager;


class Application extends SilexApplication
{
    public function __construct(array $values = []){
        parent::__construct($values);
        $this->config = $values;

        $this->registerEnvironmentParams();
        $this->registerServiceProviders();

        $db_config = new \Doctrine\DBAL\Configuration();

        $connectionParams = $this->config['database'];
        $this->db = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $db_config);

        LoginBuilder::mountProviderIntoApplication('/auth', $this);

        $this->after(function (Request $request, Response $response) {
            $response->headers->set('Access-Control-Allow-Origin', '*');
        });

        $this->before(function (Request $request) {
            if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
                $data = json_decode($request->getContent(), true);
                $request->request->replace(is_array($data) ? $data : array());
            }

            $this->db->beginTransaction();
            $this->db->insert('requests_log', ['req_body' => $request->getContent(), 'req_params' => "", 'req_query' => $request->getQueryString(), 'req_headers' => serialize($request->headers)]);
            $this->db->commit();
        });

    }

    private function registerEnvironmentParams(){
        $environment = $this->config['environment'];

        switch ($environment)
        {
            case "dev":
                $this['debug'] = true;
                break;
            case "live":
                ini_set('display_errors', false);
                break;
            default:
                throw new \RuntimeException(sprintf("Environment should be 'dev' or 'live', '%s' given", $environment));
                break;
        }
    }

    private function registerServiceProviders(){
        $app = $this;

        $app->register(new SilexProvider\SessionServiceProvider(), array('session.storage.save_path' => $app->config['root'] . '/sessions_storage'));

        $app->before(function() use ($app){
            $app['session']->start();
        });


        $app->register(new TwigServiceProvider(), [
            'debug' => true,
            'twig.path'    => $this->config['root'] . '/web/views',
            'twig.options' => ['debug' => (($this->config['environment'] === "dev") ? true : false)]
        ]);

        $twig = $app['twig'];

        $twig->addFunction(new TwigFunction('is_granted', function($role) use ($app) {
            $user = array();
            try {
                $user = $app['session']->get('user');
            }catch (Exception $e) {
                $user =  array('username' => 'anonymous', 'id' => 0, 'access_token' => '');
                $app['session']->set('user', array('username' => 'anonymous', 'id' => 0, 'access_token' => ''));
            }
            if ($user['id']) return true;
            else return false;
        }));

    }
}