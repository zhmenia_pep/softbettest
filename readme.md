SoftBetTest API deployment manual (Composer, Silex, Twig, Doctrine DBAL):
==============================

Deployment manual
-------------

# Install necessary system packages:

$ sudo yum -y update

$ sudo yum -y install composer mc curl httpd mariadb-server mariadb mariadb-client php php-pdo php-mysql php-mbstring php-xml php-curl php-gd php-memcache php-soap php-cli mod_fastcgi git ntp memcached nginx


#Set services to autoload
$ sudo systemctl enable mariadb.service

$ sudo systemctl enable httpd.service

$ sudo systemctl enable ntpd.service

$ sudo systemctl enable  memcached.service

$ sudo systemctl enable nginx

$ sudo service ntpd restart


# Start DB and set passwords
$ sudo systemctl restart mariadb.service

$ mysqladmin -u root password r00t_passwd

$ mysql -u root -p

> create database softbettest;
>
> grant usage on *.* to softbettest@localhost identified by 'some_passwd_goes_here';
>
> grant all privileges on softbettest.* to softbettest@localhost ;
>
> quit

$ sudo mysql_secure_installation


# Goto the apache DOCUMENT_ROOT and cleanup it
$ useradd -g apache softbettest

$ su softbettest
$ cd ~
$ cd public_html

# Deploy project file from GIT repository from

$ https://zhmenia_pep@bitbucket.org/zhmenia_pep/softbettest.git

# Run composer to install dependencies
$ composer install

# Import DB schema and default data:
$ gzip -dc ./private/db/softbettest.sql.gz | mysql -usoftbettest -p some_passwd_goes_here


# Set RW permissions to Apache user

$ chmod -R 0775 ./private/auto/crond/


# Configure and start Apache

…..set appropriate Apache/Nginx settings into appropriate config file

## Set default timezone into php.ini

$ sudo systemctl restart httpd.service


## Configure and start Nginx

$ sudo rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm

$ sudo yum installl nginx

$ sudo systemctl enable nginx.service

…..set appropriate configs (on port 80 in order to be proxy for the static files), sample is in ./private/conf/


$ sudo systemctl restart nginx.service


# Add tasks to Crond

47 23 */2 * * /home/softbettest/private/crond/calc_transactions_sums.sh > /home/softbettest/private/crond/calc_transactions_sums.log 2>&1 &


# Third-party libs/APIs configuration


**Done**