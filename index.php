<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include __DIR__ . '/vendor/autoload.php';


$parser = new Symfony\Component\Yaml\Yaml();
$global = $parser->parse(__DIR__ . '/config/global.yml');
$config = $parser->parse(sprintf(__DIR__ . '/config/%s.yml', $global['environment']));
$global['root'] = __DIR__;
$global['index_pathes'] = array("logout" => $global['web_root'].'/logout', "login" => $global['web_root'].'/login', "signup" => $global['web_root'].'/signup', "transactions" =>  $global['web_root'].'/transactions', "about" =>  $global['web_root'].'/about');
$global['anonymous_user'] = array('username' => 'anonymous', 'id' => 0, 'access_token' => '');

$global['models_root'] = $global['root'] . '/src/App/Models';

$resp_template = array(
    'code'      => $global['resp_ok_code'],
    'type'      => $global['resp_ok_type'],
    'data'      => array('message' => $global['resp_ok_message'])
);

include __DIR__ . '/src/InjectorServiceProvider.php';

use App\Silex\Application as Application;
use Injector\InjectorServiceProvider as InjectorServiceProvider;
use App\Silex\Provider\Login\ServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

include __DIR__ . '/src/App/Models/Users.php';
use UsersModel;

include __DIR__ . '/src/App/Models/Transactions.php';
use TransactionsModel;


$app = new Application(array_merge($global, $config));
$app['debug'] = true;

$app->register(new Moust\Silex\Provider\CacheServiceProvider(), array(
    'cache.options' => array('driver' => 'memcached')
));

$app->get('/', function (Application $app, Request $request) use ($global) {
    $user =  $global['anonymous_user'];
    $twig_params = array("user" => $user, "path" => $global['index_pathes']);
    try {
        $user = $app['session']->get('user');
        if ($user == null) throw new Exception('user == null');
        $twig_params['user'] = $user;
    }catch (Exception $e) {
        $app['session']->set('user', array('username' => 'anonymous', 'id' => 0, 'access_token' => ''));
    }

    return $app['twig']->render('index.html.twig', $twig_params);
});

$app->get('/login', function (Application $app) {
    return $app['twig']->render('login.html.twig', array('error' => false));
});

$app->get('/signup', function (Application $app) {
    return $app['twig']->render('signup.html.twig', array('error' => false));
});

$app->get('/logout', function () use ($app, $global) {
    $twig_params = array("user" => $global['anonymous_user'], "path" => $global['index_pathes']);
    $app['session']->clear();
    return $app['twig']->render('index.html.twig', $twig_params);
});

$app->get('/transactions', function (Request $request) use ($app, $global) {
    $user =  $global['anonymous_user'];
    $twig_params = array("user" => $user, "path" => $global['index_pathes'], 'error' => false, 'transactions' => array(), 'transactionsTotal' => 0);
    try {
        $user = $app['session']->get('user');
        if ($user == null) throw new Exception('user == null');
        $twig_params['user'] = $user;
        parse_str($request->getQueryString(), $params);
        $transactions_resp = $app['transactions']->getTransactions($params);
        $twig_params['transactions'] = empty($transactions_resp['err']) ? $transactions_resp['transactions'] : array();
        $twig_params['transactionsTotal'] = empty($transactions_resp['err']) ? $transactions_resp['transactionsTotal'] : 0;
        $twig_params['pagesCount'] = empty($transactions_resp['err']) ? $transactions_resp['pagesCount'] : 1;
        $twig_params['offset'] = empty($transactions_resp['err']) ? $transactions_resp['offset'] : 0;
        $twig_params['params'] = $params;
    }catch (Exception $e) {
        $app['session']->set('user', array('username' => 'anonymous', 'id' => 0, 'access_token' => ''));
    }
    return $app['twig']->render('transactions.html.twig', $twig_params);
});

$app->get('/about', function () use ($app) {
    return $app['twig']->render('about.html.twig', array());
});

$app->register(new InjectorServiceProvider([
    'UsersModel\Users' => 'users',
]));

$app['users'] = function (Application $app) {
    return new UsersModel\Users($app);
};

$app['auth'] = $app->share(function () {
    return new LoginServiceProvider();
});

$app->post('/api/users/register', function (Request $request) use ($app, $resp_template, $global) {
    $resp = $resp_template;
    $params = json_decode($request->getContent(), true);
    if (empty($params)) parse_str($request->getContent(), $params);

    $result = $app['users']->register($params);
    if (intval($result['user_id']) && intval($result['user_id']) > 0) {
        $params['id'] = intval($result['user_id']);
        $is_valid_user = $app['auth.validate.credentials']($params['username'], $params['password'], $app);
        if ($is_valid_user) {
            $resp['data']['user'] = array('username' => $params['username'], 'id' => $params['id'], 'access_token' => $app['auth.new.token']($params, $app));
            $app['session']->set('user', $resp['data']['user']);
            if (!empty($params['signup_form']))
                return $app['twig']->render('index.html.twig', array("user" => $resp['data']['user'], "path" => $global['index_pathes']));
            else return $app->json($resp);
        }else {
            $resp['code'] = 2002;
            $resp['type'] = $global['resp_err_type'];
            $resp['data']['message'] = "Error while validation of signup process";
            if (!empty($params['signup_form'])) return $app['twig']->render('signup.html.twig', array('error' => $resp['data']['message']));
            else return $app->json($resp);
        }
    } else{
        $resp['code'] = 2800;
        $resp['type'] = $global['resp_err_type'];
        $resp['data']['message'] = $result['err_m'];
        if (!empty($params['signup_form'])) return $app['twig']->render('signup.html.twig', array('error' => $resp['data']['message']));
        else return $app->json($resp);
    }
});

$app->register(new InjectorServiceProvider([
    'TransactionsModel\Transactions' => 'transactions',
]));

$app['transactions'] = function (Application $app) {
    return new TransactionsModel\Transactions($app);
};

function resp_modifier($result, $template, $global) {
    $resp = $template;
    $resp['code'] = empty($result['err']) ? 1000 : $result['err'];
    $resp['type'] = empty($result['err']) ? $global['resp_ok_type'] : $global['resp_err_type'];
    $resp['data']['message'] = empty($result['err']) ? $global['resp_ok_message'] : $result['err_m'];
    return $resp;
}

$app->get('/api/transactions', function (Application $app, Request $request)  use ($app, $resp_template, $global) {
    parse_str($request->getQueryString(), $params);
    $result = $app['transactions']->getTransactions($params);
    $resp = resp_modifier($result, $resp_template, $global);
    $resp['data']['transactions'] = empty($result['err']) ? $result['transactions'] : array();
    $resp['data']['transactionsTotal'] = empty($result['err']) ? $result['transactionsTotal'] : 0;
    $resp['data']['pagesCount'] = empty($result['err']) ? $result['pagesCount'] : 1;
    $resp['data']['offset'] = empty($result['err']) ? $result['offset'] : 0;
    $resp['data']['params'] = $params;
    return $app->json($resp);
});

$app->get('/api/transactions/{customer_id}', function (Application $app, Request $request, $customer_id)  use ($app, $resp_template, $global) {
    parse_str($request->getQueryString(), $params);
    $params['customerId'] = $customer_id;
    $result = $app['transactions']->getTransactions($params);
    $resp = resp_modifier($result, $resp_template, $global);
    $resp['data']['transactions'] = empty($result['err']) ? $result['transactions'] : array();
    $resp['data']['transactionsTotal'] = empty($result['err']) ? $result['transactionsTotal'] : 0;
    $resp['data']['pagesCount'] = empty($result['err']) ? $result['pagesCount'] : 1;
    $resp['data']['offset'] = empty($result['err']) ? $result['offset'] : 0;
    $resp['data']['params'] = $params;
    return $app->json($resp);
});

$app->get('/api/transactions/{customer_id}/{id}', function (Application $app, Request $request, $customer_id, $id)  use ($app, $resp_template, $global) {
    parse_str($request->getQueryString(), $params);
    $params['customerId'] = $customer_id;
    $params['transactionId'] = $id;
    $result = $app['transactions']->getTransactions($params);
    $resp = resp_modifier($result, $resp_template, $global);
    $resp['data']['transactions'] = empty($result['err']) ? $result['transactions'] : array();
    $resp['data']['transactionsTotal'] = empty($result['err']) ? $result['transactionsTotal'] : 0;
    $resp['data']['pagesCount'] = empty($result['err']) ? $result['pagesCount'] : 1;
    $resp['data']['offset'] = empty($result['err']) ? $result['offset'] : 0;
    $resp['data']['params'] = $params;
    return $app->json($resp);
});

$app->post('/api/transactions', function (Application $app, Request $request) use ($app, $resp_template, $global) {
    $params = json_decode($request->getContent(), true);
    $user = $app['session']->get('user');
    $result = $app['transactions']->addTransaction($params, $user);
    if (intval($result['transactionId'])) {
        $resp = $resp_template;
        $resp['data']['transactionId'] = intval($result['transactionId']);
        return $app->json($resp);
    } else{
        $resp = resp_modifier($result, $resp_template, $global);
        return $app->json($resp);
    }
});

$app->put('/api/transactions/{id}', function (Application $app, Request $request, $id)  use ($app, $resp_template, $global){
    $params = json_decode($request->getContent(), true);
    $user = $app['session']->get('user');
    $result = $app['transactions']->updateTransaction($params, $user);
    $resp['code'] = empty($result['err']) ? 1000 : $result['err'];
    $resp['type'] = empty($result['err']) ? $global['resp_ok_type'] :$global['resp_err_type'];
    $resp['data']['message'] = empty($result['err']) ? $global['resp_ok_message'] : $result['err_m'];
    $resp['data']['transaction'] = empty($result['err']) ? $result['transaction'] : array();
    return $app->json($resp);
});

$app->delete('/api/transactions/{id}', function (Application $app, $id) use ($app, $resp_template, $global){
    $result = $app['transactions']->delTransaction($id);
    $resp = resp_modifier($result, $resp_template, $global);
    return $app->json($resp);
});

$app->post('/api/transactions/calc_transactions_sums', function (Application $app) use ($app, $resp_template, $global){
    $user = $app['session']->get('user');
    $result = $app['transactions']->calcSumsOfTransactionsOverPrevDay($user);
    $resp = resp_modifier($result, $resp_template, $global);
    return $app->json($resp);
});

$app->run();
